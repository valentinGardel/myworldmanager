package fr.minecraft.myworldmanager.Utils;

public class GameruleEnum {
	public static final String
		CREEPER_GRIEFING = "CREEPER_GRIEFING",
		GHAST_GRIEFING = "GHAST_GRIEFING",
		ENDERMAN_GRIEFING = "ENDERMAN_GRIEFING",
		RAVAGER_GRIEFING = "RAVAGER_GRIEFING",
		TNT_EXPLOSION = "TNT_EXPLOSION";
	
	public static String[] GetAll()
	{
		return new String[] {
				CREEPER_GRIEFING, 
				GHAST_GRIEFING,
				ENDERMAN_GRIEFING,
				RAVAGER_GRIEFING,
				TNT_EXPLOSION
			};
	}
}
