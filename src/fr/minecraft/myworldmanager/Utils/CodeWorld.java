package fr.minecraft.myworldmanager.Utils;

public enum CodeWorld {
	Success,
	WorldAlreadyExists,
	WorldNotFound,
	Failed
}
