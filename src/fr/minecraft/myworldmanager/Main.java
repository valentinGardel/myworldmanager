package fr.minecraft.myworldmanager;

import Commons.MinecraftPlugin;
import fr.minecraft.myworldmanager.Commands.*;
import fr.minecraft.myworldmanager.Listeners.*;
import fr.minecraft.myworldmanager.Models.CustomGamerule;
import fr.minecraft.myworldmanager.Models.CustomWorld;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyWorldManager";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				WorldManager.class,
				TpWorld.class,
				GenerateAuto.class,
				ToggleDimensions.class,
				WorldDifficulty.class,
				GameruleCustomCommand.class
			};
		LISTENERS = new Class[]{
				WorldListener.class
			};
		CONFIG = Config.class;
	}

	@Override
	protected void initDatabase() throws Exception
	{
		CustomWorld.CreateTable();
		CustomGamerule.CreateTable();
	}
}