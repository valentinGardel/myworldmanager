package fr.minecraft.myworldmanager.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

import ActiveRecord.Model;
import fr.minecraft.myworldmanager.Config;
import fr.minecraft.myworldmanager.Utils.CodeWorld;

import org.bukkit.World.Environment;

public class CustomWorld extends Model
{
	private static String TABLE_NAME="CustomWorld";

	private int id;
	public String name;
	public Environment environment;
	public boolean canGenerateStructure;
	public WorldType type;
	public Long seed;
	public Difficulty difficulty;

	public CustomWorld(String name, Long seed, Environment environment, boolean canGenerateStructure, WorldType type, Difficulty difficulty)
	{
		this.id=-1;
		this.name = name;
		this.environment = environment;
		this.canGenerateStructure = canGenerateStructure;
		this.seed = seed;
		this.type = type;
		this.difficulty=difficulty;
	}

	private CustomWorld(int id, String name, Long seed, Environment environment,  boolean canGenerateStructure, WorldType type, Difficulty difficulty)
	{
		this.id=id;
		this.name = name;
		this.environment = environment;
		this.canGenerateStructure = canGenerateStructure;
		this.seed = seed;
		this.type = type;
		this.difficulty=difficulty;
	}

	public CodeWorld save()
	{
		if(id<0)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_NAME+" (name, environment, canGenerateStructure, type, seed, difficulty) values(?, ?, ?, ?, ?, ?);");
				st.setString(1, this.name);
				st.setString(2, this.environment.name());
				st.setBoolean(3, this.canGenerateStructure);
				st.setString(4, this.type.name());
				st.setLong(5, this.seed);
				st.setString(6, this.difficulty.name());
				st.executeUpdate();
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return CodeWorld.WorldAlreadyExists;
			}
		}
		else
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("UPDATE "+TABLE_NAME+" SET difficulty=? where id=?");
				st.setString(1, this.difficulty.name());
				st.setInt(2, this.id);
				st.executeUpdate();
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return CodeWorld.Failed;
			}
		}
		return CodeWorld.Success;
	}

	public CodeWorld create()
	{
		World world = Bukkit.getWorld(this.name);
		if(world==null)
		{
			WorldCreator worldCreator = null;

			worldCreator = new WorldCreator(this.name);
			worldCreator.type(this.type);
			worldCreator.generateStructures(this.canGenerateStructure);
			worldCreator.environment(this.environment);
			worldCreator.seed(this.seed);
			world = worldCreator.createWorld();
		}
		world.setAutoSave(true);
		world.setDifficulty(this.difficulty);
		world.save();
		
		return CodeWorld.Success;
	}

	public static List<CustomWorld> GetWorlds()
	{
		List<CustomWorld> worlds = new ArrayList<CustomWorld>();

		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select id, name, environment, canGenerateStructure, type, seed, difficulty from "+TABLE_NAME+";");
			ResultSet res = st.executeQuery();

			while(res.next())
				worlds.add(new CustomWorld(
						res.getInt("id"),
						res.getString("name"),
						res.getLong("seed"),
						Environment.valueOf(res.getString("environment")),
						res.getBoolean("canGenerateStructure"),
						WorldType.valueOf(res.getString("type")),
						Difficulty.valueOf(res.getString("difficulty"))
						));
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return worlds;
	}

	public static CustomWorld GetWorld(String name)
	{
		CustomWorld world = null;

		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select id, name, environment, canGenerateStructure, type, seed, difficulty from "+TABLE_NAME+" where name=?;");
			st.setString(1, name);
			ResultSet res = st.executeQuery();

			while(res.next())		
				world = new CustomWorld(
						res.getInt("id"),
						res.getString("name"),
						res.getLong("seed"),
						Environment.valueOf(res.getString("environment")),
						res.getBoolean("canGenerateStructure"),
						WorldType.valueOf(res.getString("type")),
						Difficulty.valueOf(res.getString("difficulty"))
						);
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return world;
	}

	public static void CreateTable()
	{
		try {
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("create table if not exists "+TABLE_NAME+"(id int primary key auto_increment, name varchar(50) unique, environment varchar(20), canGenerateStructure bool, type varchar(20), seed BIGINT, difficulty varchar(20));");
			stmt.close();
		}catch(SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
	}

	public void remove() {
		PreparedStatement st;
		try {
			st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_NAME+" where id=?;");
			st.setInt(1, this.id);
			st.executeUpdate();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
	}

}
