package fr.minecraft.myworldmanager.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import ActiveRecord.Model;
import fr.minecraft.myworldmanager.Config;
import fr.minecraft.myworldmanager.Utils.CodeWorld;

public class CustomGamerule  extends Model
{
	private static String TABLE_NAME="CustomGamerule";
	
	private double value;
	private String rule;
	private UUID world;
	
	public CustomGamerule(String rule, UUID world, double value)
	{
		this.rule = rule;
		this.world = world;
		this.value = value;
	}
	
	public static void CreateTable()
	{
		try {
			Statement stmt = Config.GetConfig().getConnection().createStatement();
			stmt.executeUpdate("create table if not exists "+TABLE_NAME+"(rule varchar(50), world varchar(50), value decimal(9,2), primary key (rule, world));");
			stmt.close();
		}catch(SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
	}
	
	public static CustomGamerule Get(UUID uid, String creeperGriefing)
	{
		CustomGamerule rule = null;

		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select rule, world, value from "+TABLE_NAME+" where world=? and rule=?;");
			st.setString(1, uid.toString());
			st.setString(2, creeperGriefing);
			ResultSet res = st.executeQuery();

			if(res.next())
			{
				rule = new CustomGamerule(
						res.getString("rule"),
						UUID.fromString(res.getString("world")),
						res.getDouble("value")
						);
			}
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return rule;
	}

	public CodeWorld save()
	{
		try {
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("INSERT INTO "+TABLE_NAME+" (rule, world, value) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE value = ?;");
			st.setString(1, this.rule);
			st.setString(2, this.world.toString());
			st.setDouble(3, this.value);
			st.setDouble(4, this.value);
			st.executeUpdate();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
			return CodeWorld.Failed;
		}
		return CodeWorld.Success;
	}
	
	public double getValue() {
		return this.value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
}
