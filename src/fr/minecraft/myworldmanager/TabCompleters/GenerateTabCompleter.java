package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class GenerateTabCompleter extends MinecraftTabCompleter
{
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		switch(args.length)
		{
		case 1:
			res=new ArrayList<String>();
			for(World world: Bukkit.getWorlds())
				if(world.getName().startsWith(args[0]))
					res.add(world.getName());
			break;
		case 2:
			res=new ArrayList<String>();
			res.add("[<chunck amount>]");
			break;
		case 3:
			res=new ArrayList<String>();
			res.add("[<chunk de d�part>]");
			break;
		default:
			res=null;
		}
		return res;
	}
}