package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.myworldmanager.Utils.GameruleEnum;

public class GameruleCustomTabCompleter extends MinecraftTabCompleter
{
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		switch(args.length)
		{
			case 1:
				res=new ArrayList<String>();
				for(String rule: GameruleEnum.GetAll())
					if(rule.startsWith(args[0].toUpperCase()))
						res.add(rule);
				break;
			case 2:
				res=new ArrayList<String>();
				res.add("[<value>]");
				break;
			case 3:
				res=new ArrayList<String>();
				for(World world : Bukkit.getWorlds())
					res.add(world.getName());
				break;
			default:
				res=null;
		}
		return res;
	}
}
