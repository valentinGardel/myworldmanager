package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Difficulty;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;
import fr.minecraft.myworldmanager.Models.CustomWorld;

public class WorldDifficultyTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label,  String[] args)
	{
		List<String> res=null;
		
		if(args.length==1)
		{
			res = new ArrayList<String>();
			for(CustomWorld world : CustomWorld.GetWorlds())
			{
				if(world.name.startsWith(args[0]))
					res.add(world.name);
			}
		}
		else if(args.length==2)
		{
			res = new ArrayList<String>();
			for(Difficulty dif : Difficulty.values())
			{
				if(dif.name().startsWith(args[1]))
					res.add(dif.name());
			}
		}
		
		return res;
	}
}
