package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;

public class TpWorldTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = new ArrayList<String>();
		if(sender instanceof Player)
		{
			switch(args.length)
			{
			case 1:
				res = this.getWorlds(args[0]);
				break;
			case 2:
				res.add("<x>");
				break;
			case 3:
				res.add("<y>");
				break;
			case 4:
				res.add("<z>");
				break;
			case 5:
				res = null;
				break;
			default:
				break;
			}

		}
		return res;
	}

	private List<String> getWorlds(String w) {
		List<String> res = new ArrayList<String>();
		
		for(World world : Bukkit.getWorlds())
		{
			if(world.getName().startsWith(w))
				res.add(world.getName());
		}
		
		return res;
	}

}
