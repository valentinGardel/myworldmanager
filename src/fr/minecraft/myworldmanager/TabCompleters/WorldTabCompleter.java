package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Difficulty;
import org.bukkit.World.Environment;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.myworldmanager.Models.CustomWorld;

public class WorldTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		if(sender instanceof Player)
		{
			if(label.equalsIgnoreCase("createworld"))
			{
				switch(args.length)
				{
				case 1:
					res = new ArrayList<String>();
					res.add("name");
					break;
				case 2: 
					res = new ArrayList<String>();
					res.add("seed");
					break;
				case 3:
					res = this.getEnvironments(args[2]);
					break;
				case 4:
					res = new ArrayList<String>();
					res.add("true");
					res.add("false");
					break;
				case 5:
					res = this.getTypes(args[4]);
					break;
				case 6:
					res = this.getDifficulties(args[5]);
					break;
				default:
					break;
				}
			}
			else if(label.equalsIgnoreCase("removeworld"))
			{
				switch(args.length)
				{
				case 1:
					res = this.getWorlds(args[0]);
					break;
				default:
					break;
				}
			}
		}
		return res;
	}
	
	private List<String> getDifficulties(String d)
	{
		List<String> res = new ArrayList<String>();

		for(Difficulty difficulty : Difficulty.values())
		{
			if(difficulty.name().startsWith(d))
				res.add(difficulty.name());
		}
		return res;
	}

	private List<String> getWorlds(String w) {
		List<String> res = new ArrayList<String>();
		
		for(CustomWorld customWorld : CustomWorld.GetWorlds())
		{
			if(customWorld.name.startsWith(w))
				res.add(customWorld.name);
		}
		
		return res;
	}

	private List<String> getTypes(String t)
	{
		List<String> res = new ArrayList<String>();

		for(WorldType type : WorldType.values())
		{
			if(type.name().startsWith(t))
				res.add(type.name());
		}
		return res;
	}

	private List<String> getEnvironments(String env)
	{
		List<String> res = new ArrayList<String>();

		for(Environment environment : Environment.values())
		{
			if(environment.name().startsWith(env))
				res.add(environment.name());
		}
		return res;
	}

}
