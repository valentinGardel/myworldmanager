package fr.minecraft.myworldmanager.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class ToggleDimensionsTabCompleter extends MinecraftTabCompleter
{
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		switch(args.length)
		{
		case 1:
			res=new ArrayList<String>();
			res.add("NETHER");
			res.add("END");
			break;
		default:
			res=null;
		}
		return res;
	}
}