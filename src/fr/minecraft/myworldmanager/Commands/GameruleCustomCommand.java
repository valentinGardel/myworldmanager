package fr.minecraft.myworldmanager.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.Models.CustomGamerule;
import fr.minecraft.myworldmanager.TabCompleters.GameruleCustomTabCompleter;

public class GameruleCustomCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "gamerulecustom";
		TAB_COMPLETER = GameruleCustomTabCompleter.class;
	}
	
	/**
	 * gamerulecustom [rule] [value] [world]
	 */
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		switch(args.length)
		{
			case 1:
				this.getInfo(sender, args);
				break;
			case 2:
			case 3:
				this.setGamerule(sender, args);
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Commande invalide!");
				break;
		}
		return true;
	}
	
	private void getInfo(CommandSender sender, String[] args)
	{
		if(sender instanceof Player)
		{
			String rule_name = args[0].toUpperCase();
			Player player = (Player) sender;
			CustomGamerule rule = CustomGamerule.Get(player.getLocation().getWorld().getUID(), rule_name);
			if(rule != null)
				player.sendMessage(ChatColor.GOLD+"La gamerule "+rule_name+" = "+rule.getValue());
			else
				player.sendMessage(ChatColor.GOLD+"La gamerule custom n'existe pas dans ce monde!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide pour un nom joueur!");
	}
	
	private void setGamerule(CommandSender sender, String[] args)
	{
		String rule_name = args[0].toUpperCase();
		World world = null;
		if(args.length == 3)
		{
			world = Bukkit.getWorld(args[2]);
		}
		else
		{
			if(sender instanceof Player)
			{
				world = ((Player) sender).getLocation().getWorld();
			}
		}
		if(world != null)
		{
			CustomGamerule rule = CustomGamerule.Get(world.getUID(), rule_name);
			if(rule == null)
			{
				rule = new CustomGamerule(rule_name, world.getUID(), 0);
			}
			try {
				double value = Double.parseDouble(args[1]);
				rule.setValue(value);
				switch(rule.save())
				{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"La gamerule a bien �t� modifi�!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Erreur pendant la sauvegarde de la gamerule!");
						break;
				}
			} catch(NumberFormatException ex) {
				sender.sendMessage(ChatColor.RED+"La valeur est invalide!");
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Commande invalide pour un nom joueur!");
		}
	}
}
