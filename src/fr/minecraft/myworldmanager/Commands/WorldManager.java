package fr.minecraft.myworldmanager.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World.Environment;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.Models.CustomWorld;
import fr.minecraft.myworldmanager.TabCompleters.WorldTabCompleter;
import fr.minecraft.myworldmanager.Utils.CodeWorld;

public class WorldManager extends MinecraftCommand
{
	static {
		COMMAND_NAME = "createworld";
		TAB_COMPLETER = WorldTabCompleter.class;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(label.equalsIgnoreCase("createworld"))
		{
			this.onCreateWorld(sender, args);
		}
		else if(label.equalsIgnoreCase("removeworld"))
		{
			this.onRemoveWorld(sender, args);
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		}
		return true;
	}

	/**
	 * Methode pour supprimer un monde
	 * @param sender sender de la commande
	 * @param args arguments de la commande
	 */
	private void onRemoveWorld(CommandSender sender, String[] args) {
		if(args.length == 1)
		{
			CustomWorld world = CustomWorld.GetWorld(args[0]);

			if(world!=null)
			{
				world.remove();
				sender.sendMessage(ChatColor.GREEN+"Le monde "+args[0]+" a �t� supprim� et ne serat plus disponible lors du prochain reboot serveur.");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le monde "+args[0]+" n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	/**
	 * Methode pour cr�er un monde
	 * @param sender sender de la commande
	 * @param args arguments de la commande
	 */
	//createworld [name] [seed] [environment] <cangeneratestructure> <type>
	private void onCreateWorld(CommandSender sender, String[] args)
	{
		//verif arg length
		if(args.length >= 3 && args.length <= 6)
		{
			String name = args[0];
			Long seed;
			//try parse seed
			try {
				seed = Long.parseLong(args[1]);
			} catch(NumberFormatException e) {
				sender.sendMessage(ChatColor.RED+"Seed invalide");
				return;
			}

			Environment environment = Environment.valueOf(args[2]);
			if(this.nameIsValid(args[0], environment))
			{
				boolean canGenerateStructure = (args.length>=4)?Boolean.parseBoolean(args[3]):true;
				WorldType type = (args.length>=5)?WorldType.valueOf(args[4]):WorldType.NORMAL;
				Difficulty difficulty =(args.length>=6)?Difficulty.valueOf(args[5]):Difficulty.NORMAL;

				if(name!=null && environment != null && type != null && difficulty!=null)
				{
					CustomWorld world = new CustomWorld(name, seed, environment, canGenerateStructure, type, difficulty);
					sender.sendMessage("G�n�ration du monde ...");
					CodeWorld code = world.create();
					world.save();
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le monde "+args[0]+" a �t� cr��");
						break;
					case WorldAlreadyExists:
						sender.sendMessage(ChatColor.RED+"Le monde "+args[0]+" existe d�j�!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"Commande invalide");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le nom ne correspond pas � son environment, il doit finir par _nether ou _the_end en fonction!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private boolean nameIsValid(String name, Environment environment)
	{
		if(environment==Environment.NETHER)
			return name.endsWith("_nether");
		else if(environment==Environment.THE_END)
			return name.endsWith("_the_end");
		return true;
	}
}
