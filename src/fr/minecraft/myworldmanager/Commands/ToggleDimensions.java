package fr.minecraft.myworldmanager.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.Config;
import fr.minecraft.myworldmanager.TabCompleters.ToggleDimensionsTabCompleter;

public class ToggleDimensions extends MinecraftCommand
{
	static {
		COMMAND_NAME = "enabledimension";
		TAB_COMPLETER = ToggleDimensionsTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if(args.length==1)
		{
			boolean enable = label.equalsIgnoreCase("enabledimension");

			switch(args[0].toUpperCase())
			{
			case "NETHER":
				Config.GetConfig().setEnableNether(enable);
				sender.sendMessage(ChatColor.GREEN+"Les tp aux dimensions nether est "+(enable?"activ�":"d�sactiv�")+"!");
				break;
			case "END":
				Config.GetConfig().setEnableEnd(enable);
				sender.sendMessage(ChatColor.GREEN+"Les tp aux dimensions end est "+(enable?"activ�":"d�sactiv�")+"!");
				break;
			default:
				sender.sendMessage(ChatColor.RED+"La dimension "+args[0]+" est invalide!");
				break;
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
		return true;
	}


}
