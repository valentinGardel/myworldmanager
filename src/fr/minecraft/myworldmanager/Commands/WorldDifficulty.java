package fr.minecraft.myworldmanager.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.Models.CustomWorld;
import fr.minecraft.myworldmanager.TabCompleters.WorldDifficultyTabCompleter;
import fr.minecraft.myworldmanager.Utils.CodeWorld;

public class WorldDifficulty extends MinecraftCommand
{
	static {
		COMMAND_NAME = "worlddifficulty";
		TAB_COMPLETER = WorldDifficultyTabCompleter.class;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			//worlddifficulty
			if(args.length==0)
			{
				player.sendMessage(ChatColor.GOLD+"La difficult� du monde "+player.getWorld().getName()+" est: "+player.getWorld().getDifficulty().name());
			}
			//worlddifficulty [world]
			else if(args.length == 1)
			{
				World world = Bukkit.getWorld(args[0]);
				if(world != null)
				{
					player.sendMessage(ChatColor.GREEN+"La difficult� du monde "+world.getName()+" est : "+world.getDifficulty().name());
				}
				else
					sender.sendMessage(ChatColor.RED+"Le monde "+args[0]+" n'existe pas!");
			}
			//worlddifficulty [world] [difficulty]
			else if(args.length == 2)
			{
				CustomWorld world = CustomWorld.GetWorld(args[0]);
				if(world != null)
				{
					Difficulty dif = Difficulty.valueOf(args[1]);
					if(dif != null)
					{
						world.difficulty=dif;
						CodeWorld code = world.save();
						switch(code)
						{
						case Success:
							Bukkit.getWorld(args[0]).setDifficulty(dif);
							player.sendMessage(ChatColor.GREEN+"La difficult� du monde "+player.getWorld().getName()+" a �t� pass� � : "+dif.name());
							break;
						default:
							player.sendMessage(ChatColor.RED+"Error");
							break;
						}
					}
					else
						sender.sendMessage(ChatColor.RED+"La difficult� "+args[1]+" n'existe pas!");
				}
				else
					sender.sendMessage(ChatColor.RED+"Le monde "+args[0]+" n'existe pas ou n'est pas un monde custom!");
			}
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else
			sender.sendMessage(ChatColor.RED+"Impossible pour un non joueur");
		return true;
	}

}
