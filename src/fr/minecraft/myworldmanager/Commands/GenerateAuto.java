package fr.minecraft.myworldmanager.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.TabCompleters.GenerateTabCompleter;

public class GenerateAuto extends MinecraftCommand
{
	static {
		COMMAND_NAME = "generate";
		TAB_COMPLETER = GenerateTabCompleter.class;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length == 3) {
			World w = Bukkit.getWorld(args[0]);
			if(w != null) {
				int nbChunks = Integer.parseInt(args[1]), chunkDepart = Integer.parseInt(args[2]);
				if(nbChunks > 0 && chunkDepart >= 0) {
					sender.sendMessage(ChatColor.GREEN+"generation en cours");
					this.generateWorld(w, chunkDepart ,nbChunks, sender);
					sender.sendMessage(ChatColor.GREEN+"generation fini");
				}
				else
					sender.sendMessage(ChatColor.RED+"Nombre de chunk � generer invalide");
			}
			else
				sender.sendMessage(ChatColor.RED+"Monde invalide");
		}
		else sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}

	private void generateWorld(World w, int chunkDepart, int nbChunks, CommandSender sender) {
		int x = chunkDepart, z = chunkDepart;
		for(int r = chunkDepart; r < nbChunks+chunkDepart; r++)
		{
			for(; x < r; x++)
				this.loadChunk(w, x, z, sender);
			for(; z > -r; z--)
				this.loadChunk(w, x, z, sender);
			for(; x > -r; x--)
				this.loadChunk(w, x, z, sender);
			for(; z < r; z++)
				this.loadChunk(w, x, z, sender);
		}
	}
	
	private void loadChunk(World w, int x, int z, CommandSender sender)
	{
		if(!w.isChunkGenerated(x, z)) {
			sender.sendMessage(ChatColor.GRAY+"loading chunk: ("+x+", "+z+")");
			try {
				w.loadChunk(x, z);
				wait(500);
				w.unloadChunk(x, z);
				wait(500);
			}/* catch (InterruptedException e1) {
				e1.printStackTrace();
			} */catch (Exception e2){
				e2.printStackTrace();
			}
		}
	}
}
