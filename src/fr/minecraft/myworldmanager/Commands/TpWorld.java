package fr.minecraft.myworldmanager.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.myworldmanager.TabCompleters.TpWorldTabCompleter;

public class TpWorld extends MinecraftCommand
{
	static {
		COMMAND_NAME = "tpWorld";
		TAB_COMPLETER = TpWorldTabCompleter.class;
	}
	
	public static int LIMITE_COMMANDBLOCK_RAYON = 16;

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		/* cas ou c est un joueur qui utitlise la commande */
		if(sender instanceof Player && args.length >= 1)
		{
			this.onPlayerSender(sender, args);
		}
		/* cas ou la commande est execute par un commande block */
		else if(sender instanceof BlockCommandSender && args.length >= 2 && args[0].equals("@p"))
		{
			this.onBlockCommandSender(sender, args);
		}
		else
			sender.sendMessage("commande invalide");
		return true;
	}

	private void onPlayerSender(CommandSender sender, String[] args)
	{
		Player player = (Player) sender;
		World world = Bukkit.getWorld(args[0]);
		if(world !=null) {
			if(args.length==1)
				player.teleport(world.getSpawnLocation());
			else if(args.length==4)
			{
				try {
					player.teleport(new Location(world, Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3])));
				} catch(NumberFormatException ex) {
					player.sendMessage(ChatColor.RED+"Coordonnées invalides!");
				}
			}
			else if(args.length==5)
			{
				Player targetPlayer = Bukkit.getPlayer(args[4]);
				if(targetPlayer!=null && targetPlayer.isOnline())
				{
					try {
						targetPlayer.teleport(new Location(world, Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3])));
						player.sendMessage(ChatColor.GREEN+"Joueur "+targetPlayer.getName()+" téléporté!");
					} catch(NumberFormatException ex) {
						player.sendMessage(ChatColor.RED+"Coordonnées invalides!");
					}
				}
				else
					player.sendMessage(ChatColor.RED+"Joueur "+args[4]+" introuvable!");
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			player.sendMessage(ChatColor.RED+"Le monde "+args[0]+" n'existe pas");
	}

	private void onBlockCommandSender(CommandSender sender, String[] args)
	{
		BlockCommandSender bcs = (BlockCommandSender) sender;
		Location bcsLocation = bcs.getBlock().getLocation();

		if((args.length==2 || args.length==5) && Bukkit.getWorld(args[1])!=null)
		{
			Player nearestPlayer = null;
			for(Player player : bcsLocation.getWorld().getPlayers()) {
				if(player.getLocation().distance(bcsLocation)<=LIMITE_COMMANDBLOCK_RAYON || 
						(
								nearestPlayer!=null && 
								player.getLocation().distance(bcsLocation) < nearestPlayer.getLocation().distance(bcsLocation)
								)
						)
					nearestPlayer = player;
			}
			//Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD+"Joueur le plus proche: "+nearestPlayer);
			if(nearestPlayer!=null) {
				if(args.length==2)
					nearestPlayer.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
				else if(args.length==5)
				{
					try {
					nearestPlayer.teleport(new Location(Bukkit.getWorld(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]), Double.parseDouble(args[4])));
					} catch(NumberFormatException e) {
						sender.sendMessage(ChatColor.RED+"Les coordonnées sont invalide");
					}
				}
			}
		}
	}
}
