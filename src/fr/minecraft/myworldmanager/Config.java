package fr.minecraft.myworldmanager;

import Commons.DatabaseConfig;
import org.bukkit.plugin.java.JavaPlugin;

public class Config extends DatabaseConfig
{
	private static Config instance;
	
	public void initConfig(JavaPlugin plugin)
	{
		super.initConfig(plugin);
		Config.instance = this;
	}
	
	public static Config GetConfig()
	{
		return Config.instance;
	}
	
	public boolean getUnNerfSpawner()
	{
		return this.getBoolean("unnerfSpawner");
	}
	
	public boolean getEnableEnd()
	{
		return this.getBoolean("enableEnd");
	}
	
	public void setEnableEnd(boolean enable)
	{
		this.set("enableEnd", enable);
	}
	
	public boolean getEnableNether()
	{
		return this.getBoolean("enableNether");
	}
	
	public void setEnableNether(boolean enable)
	{
		this.set("enableNether", enable);
	}
}
