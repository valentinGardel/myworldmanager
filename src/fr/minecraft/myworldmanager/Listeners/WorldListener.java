package fr.minecraft.myworldmanager.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.inventory.EntityEquipment;

import Commons.MinecraftListener;
import fr.minecraft.myworldmanager.Config;
import fr.minecraft.myworldmanager.Models.CustomGamerule;
import fr.minecraft.myworldmanager.Models.CustomWorld;
import fr.minecraft.myworldmanager.Utils.GameruleEnum;

public class WorldListener extends MinecraftListener
{
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event)
	{
		if(!event.isCancelled())
		{
			String gamerule_name = null;
			switch(event.getEntityType())
			{
			case ENDER_CRYSTAL:
			case PRIMED_TNT:
			case MINECART_TNT:
				gamerule_name = GameruleEnum.TNT_EXPLOSION;
				break;
			case CREEPER:
				gamerule_name = GameruleEnum.CREEPER_GRIEFING;
				break;
			case GHAST:
				gamerule_name = GameruleEnum.GHAST_GRIEFING;
				break;
			default:
				break;
			}
			
			if(gamerule_name != null)
			{
				CustomGamerule rule = CustomGamerule.Get(event.getLocation().getWorld().getUID(), gamerule_name);
				if(rule != null && rule.getValue() == 0)
				{
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityChangeBlock(EntityChangeBlockEvent event)
	{
		if(!event.isCancelled())
		{
			String gamerule_name = null;
			switch(event.getEntityType())
			{
			case RAVAGER:
				gamerule_name = GameruleEnum.RAVAGER_GRIEFING;
				break;
			case ENDERMAN:
				gamerule_name = GameruleEnum.ENDERMAN_GRIEFING;
				break;
			default:
				break;
			}
			
			if(gamerule_name != null)
			{
				CustomGamerule rule = CustomGamerule.Get(event.getBlock().getLocation().getWorld().getUID(), gamerule_name);
				if(rule != null && rule.getValue() == 0)
				{
					event.setCancelled(true);
				}
			}
		}
	}
	
	/**
	 *  Force mobs from spawner to have AI (bug fixing from spigot.yml file)
	 * */
	@EventHandler
	public void onSpawnerSpawning(SpawnerSpawnEvent event)
	{
		if(!event.isCancelled() && Config.GetConfig().getUnNerfSpawner() && event.getEntity() instanceof LivingEntity)
		{
			event.setCancelled(true);
			LivingEntity oldEntity = (LivingEntity) event.getEntity();
			LivingEntity newEntity = (LivingEntity) event.getLocation().getWorld().spawnEntity(event.getLocation(), event.getEntityType());
			this.setLivingEntityEquipment(oldEntity.getEquipment(), newEntity.getEquipment());
		}
	}

	/**
	 * Copy equipment
	 * @param a old equipment
	 * @param b new equipment
	 * */
	private void setLivingEntityEquipment(EntityEquipment a, EntityEquipment b)
	{
		b.setArmorContents(a.getArmorContents());
		b.setHelmetDropChance(a.getHelmetDropChance());
		b.setChestplateDropChance(a.getChestplateDropChance());
		b.setLeggingsDropChance(a.getLeggingsDropChance());
		b.setBootsDropChance(a.getBootsDropChance());
				
		b.setItemInMainHand(a.getItemInMainHand());
		b.setItemInMainHandDropChance(a.getItemInMainHandDropChance());
	}

	/**
	 * When the server start load the custom worlds
	 * */
	@EventHandler
	public void onServerReloadEvent(ServerLoadEvent event)
	{
		//load worlds at server startup
		if(event.getType() == ServerLoadEvent.LoadType.STARTUP)
		{
			//World defaultWorld = Bukkit.getWorlds().get(0), createdWorld;
			for(CustomWorld customWorld : CustomWorld.GetWorlds())
			{
				customWorld.create();
			}
		}
		else if(event.getType() == ServerLoadEvent.LoadType.RELOAD)
		{
			for(CustomWorld customWorld : CustomWorld.GetWorlds())
			{
				try {
					World world = Bukkit.getWorld(customWorld.name);
					if(world!=null)
						world.setDifficulty(customWorld.difficulty);
				} catch(NullPointerException e) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				}
			}
		}
	}

	/**
	 * End_Gateway which are auto generate are buged and teleport in the void
	 * This fix it
	 * */
	@EventHandler
	public void endPortalToEnd(PlayerTeleportEvent event) 
	{
		//Bukkit.broadcastMessage("from: "+event.getFrom()+", tp: "+event.getTo()+", cause: "+event.getCause().name());
		if(!event.isCancelled())
		{
			if(event.getCause()==TeleportCause.END_GATEWAY)
			{
				Location spawn = event.getTo().getWorld().getSpawnLocation();
				if(spawn.distance(event.getTo())<spawn.distance(event.getFrom()))
				{
					event.setTo(event.getTo().getWorld().getSpawnLocation());
					//Bukkit.broadcastMessage("relocating to the world spawn");
				}
			}
		}
	}

	/**
	 * Link end and nether portal to worlds for player
	 * */
	@EventHandler
	public void portalPlayerLink(PlayerPortalEvent event) 
	{
//		Bukkit.broadcastMessage("from: "+event.getFrom().getWorld().getName()+", tp: "+event.getTo().getWorld().getName());
		if(!event.isCancelled()){
			/* From nether to overworld */
			if(event.getFrom().getBlock().getLocation().getWorld().getEnvironment()==Environment.NETHER)
			{
				if(Config.GetConfig().getEnableNether())
				{
					World world = Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName().replace("_nether", ""));

					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						Location from = event.getFrom();
						event.setTo(new Location(world, from.getX()*8, from.getY(), from.getZ()*8));
					}
				}
				else
				{
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED+"La t�l�portation nether � �t� d�sactiv�!");
				}
			}
			/* From overworld to nether */
			else if(event.getTo().getBlock().getLocation().getWorld().getEnvironment()==Environment.NETHER)
			{
				if(Config.GetConfig().getEnableNether())
				{
					World world =Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName()+"_nether");
					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						Location from = event.getFrom(), l = new Location(world, from.getX()/8, from.getY(), from.getZ()/8);
						event.setTo(l);
						//Bukkit.broadcastMessage("loc player tp: "+l.getWorld().getName()+"("+l.getX()+", "+l.getY()+", "+l.getBlockZ()+")");
					}
				}
				else
				{
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED+"La t�l�portation nether � �t� d�sactiv�!");
				}
			}
			/* from overworld to end*/
			else if(event.getTo().getBlock().getLocation().getWorld().getEnvironment()==Environment.THE_END)
			{
				if(Config.GetConfig().getEnableEnd())
				{
					World world =Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName()+"_the_end");
					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						/*
						event.setTo(world.getSpawnLocation());
						/*/
						Location newLoc = event.getTo();
						newLoc.setWorld(world);
						event.setTo(newLoc);
						//*/
					}
				}
				else
				{
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED+"La t�l�portation nether � �t� d�sactiv�!");
				}
			}
		}
	}

	/**
	 * Link end and nether portal to worlds for entities
	 * */
	@EventHandler
	public void portalEntityLink(EntityPortalEvent event)
	{
		if(!event.isCancelled()){
			/* From nether to overworld */
			if(event.getFrom().getBlock().getLocation().getWorld().getEnvironment()==Environment.NETHER)
			{
				if(Config.GetConfig().getEnableNether())
				{
					World world = Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName().replace("_nether", ""));

					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						Location from = event.getFrom();
						event.setTo(new Location(world, from.getX()*8, from.getY(), from.getZ()*8));
					}
				}
				else
				{
					event.setCancelled(true);
				}
			}
			/* From overworld to nether */
			else if(event.getTo().getBlock().getLocation().getWorld().getEnvironment()==Environment.NETHER)
			{
				if(Config.GetConfig().getEnableNether())
				{
					World world =Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName()+"_nether");
					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						Location from = event.getFrom(), l = new Location(world, from.getX()/8, from.getY(), from.getZ()/8);
						event.setTo(l);
						//Bukkit.broadcastMessage("loc player tp: "+l.getWorld().getName()+"("+l.getX()+", "+l.getY()+", "+l.getBlockZ()+")");
					}
				}
				else
				{
					event.setCancelled(true);
				}
			}
			/* from overworld to end*/
			else if(event.getTo().getBlock().getLocation().getWorld().getEnvironment()==Environment.THE_END)
			{
				if(Config.GetConfig().getEnableEnd())
				{
					World world =Bukkit.getWorld(event.getFrom().getBlock().getWorld().getName()+"_the_end");
					if(world==null)
					{
						event.setCancelled(true);
						return;
					}
					else
					{
						/*
						event.setTo(world.getSpawnLocation());
						/*/
						Location newLoc = event.getTo();
						newLoc.setWorld(world);
						event.setTo(newLoc);
						//*/
					}
				}
				else
				{
					event.setCancelled(true);
				}
			}
		}
	}
}
